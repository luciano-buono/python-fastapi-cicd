from fastapi import FastAPI
import uvicorn
import datetime

app = FastAPI()


@app.get("/")
async def root():
    return {"message": "Hello World V3"}

@app.get("/help")
async def help():
    return({"message":"Help is coming!"})

@app.get("/date")
async def date():
    today= datetime.date.today()
    return {"Date": today}

if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port = 8080)